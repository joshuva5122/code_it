let POSTRequest = function(){
  let qname = document.querySelector('#qname').value;
  let question = document.querySelector('#question').value;
  let bPCode = document.querySelector('#bPCode').value;
  let input = document.querySelector('#input').value;
  let output = document.querySelector('#output').value;
  let xhr = new XMLHttpRequest();
  let url = `http://`+window.ip+`:8080/Code_IT/JSP/addquestion`;
  let data = JSON.stringify({qname : qname, question : question, bPCode : bPCode, input : input, output : output});
  xhr.open('POST',url); 
  xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
  xhr.onload = function(){
  let json = JSON.parse(xhr.response);
  if(json.file === 'true'){
		alert("file created");
    }
    else if(json.file === 'false'){
		alert("file creation failed");
	}
	else if(json.file === 'already exists'){
		alert("Question name already exists");
	}
  }
  xhr.send(data);
}

let POSTDelQues=function(){
	let qname = document.querySelector('#qDelete').value;
	let xhr = new XMLHttpRequest();
	let url = `http://`+ip+`:8080/Code_IT/JSP/deletequestion`;
	let data = JSON.stringify({qname : qname});
	
	xhr.open('POST',url);
	xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
	xhr.onload = function(){
  let json = JSON.parse(xhr.response);
  if(json.deleted === 'success'){
		alert("file deleted successfully");
    }
    else if(json.deleted === 'failed'){
		alert("file does not exist");
	}
	
  }
  xhr.send("data="+data);
	}
	
	let POSTDelVid=function(){
	let vname = document.querySelector('#vDelete').value;
	let xhr = new XMLHttpRequest();
	let url = `http://`+window.ip+`:8080/Code_IT/JSP/deletevideo`;
	let data = JSON.stringify({vname : vname});
	
	xhr.open('POST',url);
	xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
	xhr.onload = function(){
  let json = JSON.parse(xhr.response);
  if(json.deleted === 'success'){
		alert("video deleted successfully");
    }
    else if(json.deleted === 'failed'){
		alert("video does not exist");
	}
	
  }
  xhr.send("data="+data);
	}
document.querySelector('#submit').addEventListener('click',POSTRequest);
document.querySelector('#qDel').addEventListener('click',POSTDelQues);
document.querySelector('#vDel').addEventListener('click',POSTDelVid);