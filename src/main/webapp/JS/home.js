let resp = document.querySelector(".resp");
fetch(`http://`+window.ip+`:8080/Code_IT/JSP/givequestion`, {
  method: "POST",
})
  .then((response) => response.json())
  .then((response) => {
    console.log(response);
    let names = Object.values(response.question);
    console.log(names);
    names.forEach((id) => {
      let btn = document.createElement("button");
      btn.className = "button";
      btn.innerHTML = id;
      btn.id = id;
      resp.appendChild(btn);
    });
    response.comp.forEach((comp) => {
      document.getElementById(response.question[comp]).style.backgroundColor = "#A7F432";
    });
  });

resp.addEventListener("click", (e) => {
  if (e.target.className == "button") {
    let data = JSON.stringify({ qname: e.target.innerText });
    let xhr = new XMLHttpRequest();
    xhr.onload = () => {};

    xhr.open("POST", `http://`+ip+`:8080/Code_IT/JSP/sessionset`);
    xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhr.send("data=" + data);
    window.location.pathname = "/Code_IT/JSP/question.jsp";
  }
});
