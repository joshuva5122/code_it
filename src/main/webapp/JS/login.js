const ChangeToSignIn = document.querySelector("#sign-in-btn");
const ChangeToSignUp = document.querySelector("#sign-up-btn");
const container = document.querySelector(".container");
let signup = document.querySelector("#signUp");
let signIn = document.querySelector("#signIn");

let signUpUserName = document.querySelector("#SignupUserName");
let signUpEmail = document.querySelector("#SignupUserEmail");
let signUpPassword = document.querySelector("#SignupUserPass");

let SignInUsername = document.querySelector("#SignInUsername");
let SignInPass = document.querySelector("#SignInPass");

ChangeToSignUp.addEventListener("click", () => {
  container.classList.add("sign-up-mode");
});

ChangeToSignIn.addEventListener("click", () => {
  container.classList.remove("sign-up-mode");
});

signup.addEventListener("click", () => {
  let url = `http://`+window.ip+`:8080/Code_IT/JSP/signup`;

  let data = {
    username: signUpUserName.value,
    password: signUpPassword.value,
  };

  data = JSON.stringify(data);
  let req = new XMLHttpRequest();
  req.onload = function () {
    if (this.status == 200) {
      let res = JSON.parse(this.response);
      console.log(res);
      if (res.signup == "success") {
        window.location.reload();
      }
      if (res.signup == "failed") {
        document
          .querySelector(".usernameAlreadyExists")
          .classList.remove("hide");
      }
    }
  };
  req.open("POST", url, true);
  req.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

  req.send("data=" + data);
});
signIn.addEventListener("click", () => {
  let url = `http://`+window.ip+`:8080/Code_IT/JSP/login`;
  let data = {
    username: SignInUsername.value,
    password: SignInPass.value,
  };
  data = JSON.stringify(data);

  let req = new XMLHttpRequest();

  req.onload = function () {
    if (this.status == 200) {
      let res = JSON.parse(this.response);
      console.log(res);
      if (res.login == "success") {
        window.location.href = "../JSP/home.jsp";
      }
      if (res.login == "failed") {
        document.querySelector(".wrongPass").classList.remove("hide");
      }
    }
  };
  req.open("POST", url, true);
  req.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

  req.send("data=" + data);
});