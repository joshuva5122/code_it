let signIn = document.querySelector("#signIn");
let SignInUsername = document.querySelector("#SignInUsername");
let SignInPass = document.querySelector("#SignInPass");
signIn.addEventListener("click", () => {
  let url = `http://`+window.ip+`:8080/Code_IT/JSP/adminlogin`;
  let data = {
    adminname: SignInUsername.value,
    password: SignInPass.value,
  };
  data = JSON.stringify(data);

  let req = new XMLHttpRequest();

  req.onload = function () {
    if (req.status === 200 || req.readyState === 4) {
      let json = JSON.parse(req.response);
      if (json.login === "success") {
        window.location.pathname = "/Code_IT/JSP/admin.jsp";
      } else {
        document.querySelector(".wrongPass").classList.remove("hide");
      }
    }
  };
  req.open("POST", url);
  req.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

  req.send("data=" + data);
});