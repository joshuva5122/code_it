var confettiSettings = { target: "my-canvas", max: 1000 };
var confetti = new ConfettiGenerator(confettiSettings);
confetti.render();

let resp = document.querySelector(".resp");
fetch(`http://`+window.ip+`:8080/Code_IT/JSP/getquestion`, {
  method: "POST",
})
  .then((response) => response.json())
  .then((response) => {
    let id = Object.keys(response);

    document.querySelector(".question").innerHTML = response.question;
    document.querySelector(".questionName").innerHTML = response.qname;
    document.querySelector(".codeArea").innerText = response.BPCode;
  });

	document.querySelector(".submit").addEventListener("click", () => {
	document.querySelector(".loaderDiv").style.display = "block";
	document.querySelector("#my-canvas").classList.remove("active");
	
  console.log(document.querySelector(".codeArea").innerText);
  let data = JSON.stringify({
    code: document.querySelector(".codeArea").innerText,
  });
  console.log(data);
  let xhr = new XMLHttpRequest();
	
	
  xhr.onload = () => {

    if (xhr.status == 200) {
		console.log("loaded");
	document.querySelector(".loaderDiv").style.display = "none";

      let res = JSON.parse(xhr.response);
      if (res.output === "success") {
        document.querySelector("#my-canvas").classList.add("active");
        document.querySelector("#error").innerHTML = `<h1 style = "color: green">Success</h1>`;
      }
      if (res.output === "failed") {
	 	document.querySelector("#error").innerHTML = "Testcases Failed";
	 	
      }
      if (res.output === "malicious") {
	 	document.querySelector("#error").innerHTML = "This is Malicious code";
	 	
      }
      if (res.output === "error") {
	 	document.querySelector("#error").innerHTML = res.error;
      }
    }
  };

  xhr.open("POST", `http://`+window.ip+`:8080/Code_IT/JSP/compiler`);
  xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
  xhr.send(data);
});