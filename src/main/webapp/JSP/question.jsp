 <%@ page language="java" contentType="text/html; charset=UTF-8"
pageEncoding="UTF-8"%> <%@ page import = "Package.SessionCheck, Package.IPAddress" %>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8" />
    <title>Question</title>
    <link rel="stylesheet" href="../CSS/question.css" />
    <script src="https://code.iconify.design/2/2.2.0/iconify.min.js"></script>
  </head>

<%
String ipAddress = IPAddress.ip;
SessionCheck obj = new SessionCheck(); if(!obj.sessionCheckUsers(request)){
  response.sendRedirect("../JSP/login.jsp"); }
%>
  <body>
    <div class="loaderDiv">
      <div class="loader"></div>
    </div>
    <div class="nav">
      <div>
        <img src="../img/Code_it1-01.svg" alt="" />
      </div>
      <form action="logout" method="post">
        <button type="submit">
          <span class="iconify" data-icon="uiw:logout">Logout</span>
        </button>
      </form>
    </div>

    <div class="resp">
      <div class="questionName"></div>
      <div class="question"></div>
      <code class="codeArea" contenteditable="true"></code>
    </div>
    <button type="button" class="submit">Submit</button>
    <h1 id="error" style="color: red"></h1>
    <canvas id="my-canvas"></canvas>

    <div class="result"></div>
    <script>
    window.ip = "<%= ipAddress %>";
    </script>
    <script src="../JS/confetti.js"></script>
    <script type="text/javascript" src="../JS/question.js"></script>
  </body>
</html>