<%@ page language="java" contentType="text/html; charset=UTF-8"
pageEncoding="UTF-8"%> <%@ page import = "Package.SessionCheck, Package.IPAddress" %>

<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8" />

    <title>Home</title>
    <link rel="stylesheet" href="../CSS/home.css" />
  </head>
 <%
 String ipAddress = IPAddress.ip;
  SessionCheck obj = new SessionCheck(); 
  if(!obj.sessionCheckUsers(request)){
   response.sendRedirect("../JSP/login.jsp"); }
 %>
  <body>
    <div class="nav">
      <div>
        <img src="../img/Code_it1-01.svg" alt="" />
      </div>
      <form action="logout" method = "post">
        <button type="submit">Logout</button>
      </form>
    </div>
	<div class="tutorialsDiv">
      <div class="vdoTuts">
        <a href="videos.jsp">Video tutorial</a>
      </div>
    </div>
    <div class="resp"></div>
    <script>
    window.ip = "<%= ipAddress %>";
    </script>
    <script type="text/javascript" src="../JS/home.js"></script>
  </body>
</html>