<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="Package.IPAddress"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<script src="https://kit.fontawesome.com/64d58efce2.js"
	crossorigin="anonymous"></script>
<link rel="stylesheet" href="../CSS/login.css" />
<title>Log in</title>
</head>

<body>
	<%
	String ipAddress = IPAddress.ip;
	%>
	<div class="container">
		<div class="forms-container">
			<div class="signin-signup">
				<form class="sign-in-form">
					<h2 class="title">Sign in</h2>
					<div class="input-field">
						<i class="fas fa-user"></i> <input type="text"
							placeholder="Username" id="SignInUsername" />
					</div>
					<div class="error noAccount hide">Sorry, we could not find
						your account.</div>
					<div class="input-field">
						<i class="fas fa-lock"></i> <input type="password"
							placeholder="Password" id="SignInPass" />
					</div>

					<div class="error wrongPass hide">Password or Username is
						incorrect!</div>
					<button type="button" id="signIn" class="btn solid">Sign
						in</button>
				</form>

				<!--  -->

				<form action="#" class="sign-up-form">
					<h2 class="title">Sign up</h2>
					<div class="input-field">
						<i class="fas fa-user"></i> <input type="text"
							placeholder="Username" id="SignupUserName" />
					</div>
					<div class="invalidUsername error hide">No special characters
						allowed</div>
					<div class="usernameAlreadyExists error hide">Username
						already exists</div>
					<div class="input-field">
						<i class="fas fa-lock"></i> <input type="password"
							placeholder="Password" id="SignupUserPass" />
					</div>
					<div class="invalidPass error hide">Password or Username is
						incorrect!</div>

					<button type="button" class="btn" id="signUp" value="Sign up">
						Signup</button>
				</form>
			</div>
		</div>

		<div class="panels-container">
			<div class="panel left-panel">
				<div class="content">
					<h3>New here ?</h3>
					<p>Join our Family</p>
					<button class="btn transparent" id="sign-up-btn">Sign up</button>
				</div>
				<img src="../img/Code_it1-01.svg" class="image" alt="" />
			</div>
			<div class="panel right-panel">
				<div class="content">
					<h3>One of us ?</h3>
					<p>Sign in with ur Username!</p>
					<button class="btn transparent" id="sign-in-btn">Sign in</button>
				</div>
				<img src="../img/Code_it1-01.svg" class="image" alt="" />
			</div>
		</div>
	</div>
	<script>
		window.ip = "<%=ipAddress%>";
	</script>
	<script src="../JS/login.js"></script>
</body>
</html>