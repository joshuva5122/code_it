<%@ page language="java" contentType="text/html; charset=UTF-8"
pageEncoding="UTF-8"%><%@ page import = "Package.SessionCheck,Package.IPAddress" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Videos</title>
<link rel="stylesheet" href="../CSS/videos.css" />
</head>
<body>
 <%
 String ipAddress = IPAddress.ip;
  SessionCheck obj = new SessionCheck(); if(!obj.sessionCheckUsers(request)){
   response.sendRedirect("../HTML/login.html"); }
 %>
	<div id = "videos"></div>
	<script>
	window.ip = "<%= ipAddress %>";
    </script>
	<script src = "../JS/videos.js"></script>
</body>
</html>