<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import = "Package.SessionCheck,Package.IPAddress" %>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta
      name="viewport"
      content="width=
    , initial-scale=1.0"
    />
    <title>Document</title>
    <style>
      @import url("https://fonts.googleapis.com/css2?family=Work+Sans&display=swap");
      *,
      *::after,
      *::before {
        margin: 0;
        padding: 0;
        box-sizing: border-box;
        font: inherit;
      }
      body,
      html {
        height: 100%;
        display: grid;
        place-items: center;
        font-family: "Work Sans", sans-serif;
        color: #000;
      }
      form {
        width: min(100%, 900px);
        border-top: 1px solid #ddd;
        border-left: 1px solid #ddd;
        box-shadow: 6.5px 9px 5px 4px #ddd;

        border-radius: 20px;
        padding: 30px;
        display: flex;
        flex-direction: column;
      }
      form > * {
        /* border: 1px solid red; */
        margin: 20px 10px;
        padding: 10px;
        display: flex;
        align-items: center;
      }
      .vdoInp {
        margin: 0;
        padding: 0;
      }
      .desc {
        display: flex;
      }
      .desc input {
        flex-grow: 1;
        border: none;
        outline: none;
        padding-inline-start: 10px;
        border-bottom: 1px solid black;
      }
      button {
        text-align: center;
        display: grid;
        place-items: center;
        border: 0;
        outline: none;
        border-radius: 100px;
        background-color: #507efa;
      }
    </style>
  </head>
  <body>
<%

SessionCheck obj = new SessionCheck();
if(!obj.sessionCheckAdmin(request)){
	response.sendRedirect("../HTML/admin.html");
}
%> 
    <form action="fileupload" method="post" enctype="multipart/form-data">
      <div>
        File : &nbsp;
        <input type="file" name="filename" accept=" video/*" class="vdoInp" />
      </div>
      <div class="desc">
        Description : &nbsp;
        <input type="text" name="description" />
      </div>
      
      <button type="submit">upload</button>
    </form>
    <!-- <script src="../JS/videos.js"></script> -->
  </body>
</html>