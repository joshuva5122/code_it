<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import = "Package.SessionCheck,Package.IPAddress" %>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=, initial-scale=1.0"/>
    <link rel="stylesheet" href="../CSS/AdminScript.css" />
    <title>Document</title>
  </head>
  <body>
<%
String ipAddress = IPAddress.ip;
SessionCheck obj = new SessionCheck();
if(!obj.sessionCheckAdmin(request)){
	response.sendRedirect("../JSP/adminLogin.jsp");
}
%>
    <div class="wrapper">
      <div class="qname-div">
        <label for="Qname">Question Name</label>
        <input type="text" name="Qname" id="qname" />
      </div>
      <label for="Q">Question</label>
      <textarea id="question" name="Q"></textarea>
      <label for="Q">BP-Code</label><br />
      <textarea id="bPCode" name="Q"></textarea>
      <div class="inout">
        <div>
          <label for="input">input</label>
          <textarea name="" id="input" cols="30" rows="10"></textarea>
        </div>
        <div>
          <label for="output">output</label>
          <textarea name="" id="output" cols="30" rows="10"></textarea>
        </div>
      </div>
      <button type="button" id="submit">Submit</button>
    </div>
    <div class="files">
     <div class="addvideoDiv">
     
     <a href = "uploadVideo.jsp"><button>upload video</button></a>
        <div>
        </div>
      </div>
      <div class="deleteQuestionDiv">
        <h2>Delete question</h2>
        <p>question name :</p>
        <div><input type="text" name="qtoDele" id="qDelete"/> <button id=qDel>delete</button></div>
      </div>

      <div class="deleteVdoDiv">
        <h2>delete video</h2>
        <p>video name :</p>
        <div><input type="text" id="vDelete"/> <button id=vDel>delete</button></div>
      </div>
      <div class="showButton">
        <button>show data</button>
      </div>
    </div>
    
    <script>
    window.ip = "<%= ipAddress %>";
    </script>
    <script src="../JS/AdminScript.js"></script>
  </body>
</html>
