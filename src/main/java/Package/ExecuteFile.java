package Package;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;

public class ExecuteFile{

	public String printLines(InputStream stream) throws Exception {
        String result = "";
        String str = "";
        BufferedReader bufferReader = new BufferedReader(new InputStreamReader(stream));
        while((str = bufferReader.readLine()) != null) {
            result = result + str;
        }
        return result;
    }
    public String runProcessString(String command) throws Exception {
        String result = "";
        Process process = Runtime.getRuntime().exec(command);
        result = printLines(process.getErrorStream());
        process.waitFor();
        return result;
    }
    public void runProcess(String command) throws Exception {
        Process process = Runtime.getRuntime().exec(command);
        Thread.sleep(5000);
        process.destroyForcibly();
    }
    public int runProcessInt(String command) throws Exception {
        String result = "";
        Process process = Runtime.getRuntime().exec(command);
        result = printLines(process.getErrorStream());
        process.waitFor();
        int value = process.exitValue();
        return value;
    }
}
