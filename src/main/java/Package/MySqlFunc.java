package Package;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class MySqlFunc{
	
	public static Connection connection = null;
	
	public static void connectMySql() throws Exception{
		Class.forName("com.mysql.cj.jdbc.Driver");
		connection = DriverManager.getConnection("jdbc:mysql://localhost/Code_IT", "code_ITadmin", "code_itadmin");
	}
	public static void closeMySql() throws Exception{
		connection.close();
	}
	public static boolean signUp(String username, String password, String table_name) throws Exception{
		connectMySql();
		if(!checkData(username, table_name, "username")){
			PreparedStatement preparedstatement = connection.prepareStatement("insert into " + table_name + "(username, password) values(?, ?);");
			preparedstatement.setString(1, username);
			preparedstatement.setString(2, password);
			preparedstatement.executeUpdate();
			preparedstatement.close();
			connection.close();
			return true;
		}
		connection.close();
		return false;
		
	}
	public static boolean login(String username, String password, String table_name) throws Exception{
		connectMySql();
		if(checkData(username, table_name, "username") && checkData(password, table_name, "password")){
			connection.close();
			return true;
		}
		connection.close();
		return false;
	}
	public static boolean checkData(String data, String table_name, String column_name) throws Exception{
		PreparedStatement preparedstatement = connection.prepareStatement("select " + column_name + " from " + table_name + " where " + column_name + " = ?;");
		preparedstatement.setString(1, data);
		ResultSet resultset = preparedstatement.executeQuery();
		resultset.next();
		if(resultset.getRow() >= 1 && resultset.getString(column_name).equals(data)){
			return true;
		}
		return false;
	}
	public static boolean checkData(String data, String table_name, String column_name, int n) throws Exception{
		connectMySql();
		PreparedStatement preparedstatement = connection.prepareStatement("select " + column_name + " from " + table_name + " where " + column_name + " = ?;");
		preparedstatement.setString(1, data);
		ResultSet resultset = preparedstatement.executeQuery();
		resultset.next();
		if(resultset.getRow() >= 1 && resultset.getString(column_name).equals(data)){
			connection.close();
			return true;
		}
		connection.close();
		return false;
	}
	public static boolean addQuestion(String qname) throws Exception {
		connectMySql();
		if(!checkData(qname,"questions", "qname")){
			PreparedStatement preparedstatement = connection.prepareStatement("insert into questions (qname) values(?);");
			preparedstatement.setString(1, qname);
			preparedstatement.executeUpdate();
			preparedstatement.close();
			connection.close();
			return true;
		}
		connection.close();
		return false;
	}
	public static Map getQuestions(String table) throws Exception{
		connectMySql();
		Map<Integer, String> data = new HashMap<Integer, String>();
		PreparedStatement preparedstatement = connection.prepareStatement("select * from " + table + ";");
		ResultSet resultset = preparedstatement.executeQuery();
		while(resultset.next()) {
			data.put(resultset.getInt(1), resultset.getString(2));
		}
		connection.close();
		return data;
	}
	
	
	public static ArrayList<Integer> getCompleatedQuestions(String user) throws Exception{
		connectMySql();
		ArrayList<Integer> data=new ArrayList<>();
		String userId=userIdGetter(user);		
		PreparedStatement preparedstatement = connection.prepareStatement("select * from  user_questions where uid="+userId+";");
		ResultSet resultset = preparedstatement.executeQuery();
		while(resultset.next()) {
			data.add(resultset.getInt(2));
		}
		connection.close();
		return data;
	}
	
	public static String userIdGetter(String user) throws Exception{
		
		String str = "";
		PreparedStatement preparedstatement = connection.prepareStatement("select * from users where username like '"+user+"' ;");
		ResultSet resultset = preparedstatement.executeQuery();
		if(resultset.next()) {
			str = str + resultset.getString(1);
		}
		
		return str;
	}
	
	public static String quesIdGetter(String qname) throws Exception{
		
		String str = "";
		PreparedStatement preparedstatement = connection.prepareStatement("select * from questions where qname like '"+qname+"' ;");
		ResultSet resultset = preparedstatement.executeQuery();
		if(resultset.next()) {
			str = str + resultset.getString(1);
		}
		
		return str;
	}
	
	public static void sucessQuestionUpdate(String qname, String user) throws Exception {
		connectMySql();
		String userId=userIdGetter(user);
		String quesId=quesIdGetter(qname);
		if(! present(userId,quesId)) {
		PreparedStatement preparedstatement = connection.prepareStatement("insert into user_questions(uid, qid) values(?, ?);");
		preparedstatement.setString(1, userId);
		preparedstatement.setString(2, quesId);
		preparedstatement.executeUpdate();
		}
		connection.close();
	}
	
	public static boolean present(String uid,String qid) throws Exception{
		
		
		PreparedStatement preparedstatement = connection.prepareStatement("select * from user_questions where uid="+uid+" and qid="+qid+";");
		ResultSet resultset = preparedstatement.executeQuery();
		if(resultset.next()) {
			return true;
		}
		
		return false;
	}
	
	
	public static void fileUpload(String filename, String description) throws Exception {
		connectMySql();
		PreparedStatement preparedstatement = connection.prepareStatement("insert into files(filename, description) values(?, ?);");
		preparedstatement.setString(1, filename);
		preparedstatement.setString(2, description);
		preparedstatement.executeUpdate();
		connection.close();
	}
	public static String display(String column, String table) throws Exception{
		connectMySql();
		String str = "";
		PreparedStatement preparedstatement = connection.prepareStatement("select " + column + " from " + table + " ;");
		ResultSet resultset = preparedstatement.executeQuery();
		while(resultset.next()) {
			str = str + resultset.getString(1) + ",";
		}
		connection.close();
		return str;
	}
	public static void delete(String data, String table_name, String column_name) throws Exception{
		connectMySql();
		PreparedStatement preparedstatement = connection.prepareStatement("delete from " + table_name + " where " + column_name + " like ? ;");
		preparedstatement.setString(1, data);
		preparedstatement.executeUpdate();
		connection.close();
	}
}
