package Package;
import java.io.*;

public class QuestionPoster {
    String questionName;


    public QuestionPoster(String questionName){
        this.questionName=questionName;
    }

    public String getQuestion() throws IOException {
        File qFile=new File("/home/local/ZOHOCORP/josh-zstch1015/Code_IT_Files/Question_Folder/"+questionName+"/question.txt");

            BufferedReader reader = new BufferedReader(new FileReader (qFile));
            String         line = null;
            StringBuilder  stringBuilder = new StringBuilder();
            String         ls = System.getProperty("line.separator");

            try {
                while((line = reader.readLine()) != null) {
                    stringBuilder.append(line);
                    stringBuilder.append(ls);
                }


            } finally {
                reader.close();
            }


        return stringBuilder.toString();
    }

    public String getBPCode() throws IOException {
        File bPFile=new File("/home/local/ZOHOCORP/josh-zstch1015/Code_IT_Files/Question_Folder/"+questionName+"/BPCode.txt");

        BufferedReader reader = new BufferedReader(new FileReader (bPFile));
        String         line = null;
        StringBuilder  stringBuilder = new StringBuilder();
        String         ls = System.getProperty("line.separator");

        try {
            while((line = reader.readLine()) != null) {
                stringBuilder.append(line);
                stringBuilder.append(ls);
            }


        } finally {
            reader.close();
        }


        return stringBuilder.toString();
    }
}
