package Package;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;

import org.json.simple.JSONObject;

import Package.MySqlFunc;

@MultipartConfig
public class FileUpload extends HttpServlet{
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		response.addHeader("Access-Control-Allow-Origin", "*");
		HttpSession session  = request.getSession();
		List<Part> parts = (List) request.getParts();
		
		String filename = parts.get(0).getSubmittedFileName();
		String description = request.getParameter("description");
		try {
			if(!MySqlFunc.checkData(filename, "files", "filename", 1)) {
				parts.get(0).write("/home/local/ZOHOCORP/josh-zstch1015/Periya_project/Code_IT/src/main/webapp/media/" + parts.get(0).getSubmittedFileName());
				System.out.println("hello");
				MySqlFunc.fileUpload(filename, description);
				session.setAttribute("status", "video upploaded successfully");
				response.sendRedirect("status.jsp");
			}
			else {
				session.setAttribute("status", "file name already exists");
				response.sendRedirect("status.jsp");
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
			session.setAttribute("status", "file upload Failed!");
			response.sendRedirect("status.jsp");
		}
	}
}
