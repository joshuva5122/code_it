package Package;

import java.io.*;

public class OutputsRetriver {
    public String adminOutput(String questionName) throws IOException {
        File qFile=new File("/home/local/ZOHOCORP/josh-zstch1015/Code_IT_Files/Question_Folder/"+questionName+"/output.txt");

        BufferedReader reader = new BufferedReader(new FileReader(qFile));
        String         line = null;
        StringBuilder  stringBuilder = new StringBuilder();
        String         ls = System.getProperty("line.separator");

        try {
            while((line = reader.readLine()) != null) {
                stringBuilder.append(line);
                stringBuilder.append(ls);
            }
        } finally {
            reader.close();
        }
        return stringBuilder.toString();
    }
    public String userOutput(String userName) throws IOException {
        File qFile=new File("/home/local/ZOHOCORP/josh-zstch1015/Code_IT_Files/Users/"+userName+"/output.txt");

        BufferedReader reader = new BufferedReader(new FileReader(qFile));
        String         line = null;
        StringBuilder  stringBuilder = new StringBuilder();
        String         ls = System.getProperty("line.separator");

        try {
            while((line = reader.readLine()) != null) {
                stringBuilder.append(line);
                stringBuilder.append(ls);
            }
        } finally {
            reader.close();
        }
        return stringBuilder.toString();
    }
}

