package Package;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class UserFileCreator {

	private String userName;
    private String path;

    public UserFileCreator(String userName){
    	this.userName=userName;
        this.path="/home/local/ZOHOCORP/josh-zstch1015/Code_IT_Files/Users/"+userName;
    }

    private boolean  createUserFolder(){
        File dir =new File(path);
        return dir.mkdir();
    }

    private boolean createOutputFile(){
        File file = new File(path+"/output.txt");
        try {
            FileWriter writer = new FileWriter(file);
            writer.write(" ");
            writer.close();
            return true;
        }catch (Exception ee){
            return false;
        }
    }

    private boolean createInputFile(){
        File file = new File(path+"/input.txt");
        try {
            FileWriter writer = new FileWriter(file);
            writer.write(" ");
            writer.close();
            return true;
        }catch (Exception ee){
            return false;
        }
    }

    
    private boolean createShellScript(){
    	File file = new File(path+"/run.sh");
        try {
            FileWriter writer = new FileWriter(file);
            writer.write("cd Code_IT_Files/Users/" + userName +"/ \n java Main < input.txt > output.txt");
            writer.close();
            execute("chmod +rwx "+path+"/run.sh");
            return true;
        }catch (Exception ee){
            return false;
        }
    }
    
    public void execute(String cmd) throws IOException {
		Process process = Runtime.getRuntime().exec(cmd);
	}

    public boolean create(){
        if(createUserFolder() && createOutputFile() && createInputFile() && createShellScript()){
            return true;
        }
        return false;
    }

}