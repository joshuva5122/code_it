package Package;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import Package.MySqlFunc;

public class AdminLogin extends HttpServlet{
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
		response.addHeader("Access-Control-Allow-Origin", "*");
		String data = request.getParameter("data");
		PrintWriter printWriter = new PrintWriter(response.getWriter());
		JSONParser jsonParser = new JSONParser();
		JSONObject jsonObjectRes = new JSONObject();
		JSONObject jsonObject = null;
		HttpSession session  = request.getSession();
		MySqlFunc mySql = new MySqlFunc();
		try {
			jsonObject = (JSONObject) jsonParser.parse(data);
		}
		catch(Exception e) {
			System.out.println(e.getMessage());
		}
		String adminname = (String) jsonObject.get("adminname");
		String password = (String) jsonObject.get("password");
		try {
			if(mySql.login(adminname, password, "admin")) {
				jsonObjectRes.put("login", "success");
				session.setAttribute("adminname", adminname);
				printWriter.print(jsonObjectRes);
			}
			else {
				jsonObjectRes.put("login", "failed");
				printWriter.print(jsonObjectRes);
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}
}
