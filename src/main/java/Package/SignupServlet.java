package Package;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;



public class SignupServlet extends HttpServlet{
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
		response.addHeader("Access-Control-Allow-Origin", "*");
		String data = request.getParameter("data");
		PrintWriter printWriter = new PrintWriter(response.getWriter());
		JSONParser jsonParser = new JSONParser();
		JSONObject jsonObjectRes = new JSONObject();
		JSONObject jsonObject = null;
		HttpSession session  = request.getSession();
		MySqlFunc mySql = new MySqlFunc();
		try {
			jsonObject = (JSONObject) jsonParser.parse(data);
		}
		catch(Exception e) {
			System.out.println(e.getMessage());
		}
		String username = (String) jsonObject.get("username");
		String password = (String) jsonObject.get("password");
		try {
			if(mySql.signUp(username, password, "users")) {
				jsonObjectRes.put("signup", "success");
				session.setAttribute("username", username);
				printWriter.print(jsonObjectRes);
			}
			else {
				jsonObjectRes.put("signup", "failed");
				printWriter.print(jsonObjectRes);
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}
}
