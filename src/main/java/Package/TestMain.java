package Package;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;

import Package.MySqlFunc;

public class TestMain {
	public static void main(String[] args) throws Exception {
		System.out.println(runProcessString("pwd"));
	}
	private static String printLines(InputStream stream) throws Exception {
  		String result = "";
  		String str = "";
    	BufferedReader bufferReader = new BufferedReader(new InputStreamReader(stream));
    	while((str = bufferReader.readLine()) != null) {
        	result = result + str;
    	}
    	return result;
  	}
	private static String runProcessString(String command) throws Exception {
	    String result = "";
	    Process process = Runtime.getRuntime().exec(command);
	    result = printLines(process.getInputStream()) + "\n" +printLines(process.getErrorStream());
	    process.waitFor();
	    return result;
	  }
}
