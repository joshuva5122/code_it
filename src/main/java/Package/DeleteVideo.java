package Package;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

public class DeleteVideo extends HttpServlet{
	public void doPost(HttpServletRequest request, HttpServletResponse response) {
		response.addHeader("Access-Control-Allow-Origin", "*");
		FileDeleter fileDelete = new FileDeleter();
		String data = request.getParameter("data");
		PrintWriter printWriter;
		try {
			printWriter = new PrintWriter(response.getWriter());
		
		JSONParser jsonParser = new JSONParser();
		JSONObject jsonObjectRes = new JSONObject();
		JSONObject jsonObject = null;
		
		MySqlFunc mySql = new MySqlFunc();
		try {
			jsonObject = (JSONObject) jsonParser.parse(data);
		}
		catch(Exception e) {
			System.out.println(e.getMessage());
		}
		String vname = (String) jsonObject.get("vname");
		try {
		if(mySql.checkData(vname, "files", "filename",1)) {
			mySql.delete(vname, "files", "filename");
			fileDelete.deleteVideo(vname);
			jsonObjectRes.put("deleted", "success");
			printWriter.print(jsonObjectRes);
		}
		else {
			jsonObjectRes.put("deleted", "failed");
			printWriter.print(jsonObjectRes);
			
		}
		}catch(Exception ee) {
			System.out.println(ee.getMessage());
		}
		} catch (IOException e1) {
			e1.printStackTrace();
		}
	}
}
