package Package;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import Package.FileDeleter;

public class DeleteQuestion extends HttpServlet{
	public void doPost(HttpServletRequest request, HttpServletResponse response) {
		response.addHeader("Access-Control-Allow-Origin", "*");
		FileDeleter fileDelete = new FileDeleter();
		String data = request.getParameter("data");
		PrintWriter printWriter;
		try {
			printWriter = new PrintWriter(response.getWriter());
		
		JSONParser jsonParser = new JSONParser();
		JSONObject jsonObjectRes = new JSONObject();
		JSONObject jsonObject = null;
		
		MySqlFunc mySql = new MySqlFunc();
		try {
			jsonObject = (JSONObject) jsonParser.parse(data);
		}
		catch(Exception e) {
			System.out.println(e.getMessage());
		}
		String qname = (String) jsonObject.get("qname");
		try {
		if(mySql.checkData(qname, "questions", "qname",1)) {
			mySql.delete(qname, "questions", "qname");
			fileDelete.deleteQuestion(qname);
			jsonObjectRes.put("deleted", "success");
			printWriter.print(jsonObjectRes);
		}
		else {
			jsonObjectRes.put("deleted", "failed");
			printWriter.print(jsonObjectRes);
			
		}
		}catch(Exception ee) {
			System.out.println(ee.getMessage());
		}
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
	}
}