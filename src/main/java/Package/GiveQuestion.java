package Package;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.json.simple.JSONObject;

import Package.MySqlFunc;

public class GiveQuestion extends HttpServlet{
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
		response.addHeader("Access-Control-Allow-Origin", "*");
		PrintWriter printWriter = new PrintWriter(response.getWriter());
		HttpSession session = request.getSession();
		String username = (String)session.getAttribute("username");
		JSONObject jsonObject;
		try {
			jsonObject=new JSONObject();
			JSONObject jsonObject1 = new JSONObject(MySqlFunc.getQuestions("questions"));
			ArrayList<Integer> arr=  MySqlFunc.getCompleatedQuestions(username);
			jsonObject.put("question", jsonObject1);
			jsonObject.put("comp", arr);
			
			printWriter.print(jsonObject);
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		
	}
}
