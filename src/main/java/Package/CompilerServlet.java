package Package;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import Package.ExecuteFile;
import Package.UserFileWriter;
import Package.OutputsRetriver;
import Package.MySqlFunc;

public class CompilerServlet extends HttpServlet{
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
		response.addHeader("Access-Control-Allow-Origin", "*");
		BufferedReader br =request.getReader();
		String data=br.readLine();
		org.json.simple.parser.JSONParser parser = new org.json.simple.parser.JSONParser();
		JSONObject jsonObject=new JSONObject();
		ExecuteFile exFile = new ExecuteFile();
		PrintWriter printWriter = new PrintWriter(response.getWriter());
		JSONParser jsonParser = new JSONParser();
		JSONObject jsonObjectRes = new JSONObject();
		HttpSession session  = request.getSession();
		MySqlFunc mySql = new MySqlFunc();
		MaliciousChecker checker = new MaliciousChecker();
		OutputsRetriver outputRetriver = new OutputsRetriver();
		try {
			jsonObject = (JSONObject) jsonParser.parse(data);
		}
		catch(Exception e) {
			System.out.println(e.getMessage());
		}
		String code = (String) jsonObject.get("code");
		code = code.replace("Â", "");
		System.out.println(code);
		String username = (String) session.getAttribute("username");
		String qname = (String) session.getAttribute("qname");
		UserFileWriter userFW = new UserFileWriter(username, qname, code);
		if (checker.isSafe(code)) {
		if(userFW.writer()) {
			try {
				if(exFile.runProcessInt("javac /home/local/ZOHOCORP/josh-zstch1015/Code_IT_Files/Users/" + username +"/Main.java") != 0) {
					String error = exFile.runProcessString("javac /home/local/ZOHOCORP/josh-zstch1015/Code_IT_Files/Users/" + username +"/Main.java");
					error = error.replace("/home/local/ZOHOCORP/josh-zstch1015/Code_IT_Files/Users/final/", "\n");
					jsonObjectRes.put("output", "error");
					jsonObjectRes.put("error", error);
					printWriter.print(jsonObjectRes);
				}
				else {
					exFile.runProcess("./Code_IT_Files/Users/"+username+"/run.sh");
					String output = outputRetriver.userOutput(username);
					String outputAdmin = outputRetriver.adminOutput(qname);
					if(output.equals(outputAdmin)) {
						jsonObjectRes.put("output", "success");
						MySqlFunc.sucessQuestionUpdate(qname, username);
						printWriter.print(jsonObjectRes);
					}
					else {
						jsonObjectRes.put("output", "failed");
						printWriter.print(jsonObjectRes);
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		}
		else {
			jsonObjectRes.put("output","malicious");
			printWriter.print(jsonObjectRes);
		}
	}
}
