package Package;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.json.simple.JSONObject;

import Package.QuestionPoster;

public class GetQuestion extends HttpServlet{
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
		response.addHeader("Access-Control-Allow-Origin", "*");
		HttpSession session = request.getSession();
		String qname = (String)session.getAttribute("qname");
		String username = (String)session.getAttribute("username");
		QuestionPoster qPoster = new QuestionPoster(qname);
		JSONObject jsonObject = new JSONObject();
		PrintWriter printWriter = new PrintWriter(response.getWriter());
		jsonObject.put("qname", qname);
		jsonObject.put("question", qPoster.getQuestion());
		jsonObject.put("BPCode", qPoster.getBPCode());
		printWriter.print(jsonObject);
	}
}
