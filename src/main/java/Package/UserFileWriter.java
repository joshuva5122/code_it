package Package;

import java.io.*;

public class UserFileWriter {
    String userName;
    String input;
    String questionName;
    String javaCode;

    public UserFileWriter(String userName,String questionName, String javaCode){
        this.userName=userName;
        this.questionName=questionName;
        this.javaCode=javaCode;
    }

    public boolean writeJavaCode(){
        File file =new File("/home/local/ZOHOCORP/josh-zstch1015/Code_IT_Files/Users/"+userName+"/Main.java");
        try {
            FileWriter writer = new FileWriter(file);
            writer.write(javaCode);
            writer.close();
            return true;
        }catch (Exception ee){
            return false;
        }
    }

    public String getInput() throws IOException {
        File qFile=new File("/home/local/ZOHOCORP/josh-zstch1015/Code_IT_Files/Question_Folder/"+questionName+"/input.txt");

        BufferedReader reader = new BufferedReader(new FileReader(qFile));
        String         line = null;
        StringBuilder  stringBuilder = new StringBuilder();
        String         ls = System.getProperty("line.separator");

        try {
            while((line = reader.readLine()) != null) {
                stringBuilder.append(line);
                stringBuilder.append(ls);
            }
        } finally {
            reader.close();
        }
        return stringBuilder.toString();
    }

    public boolean writeInput(){
        File file =new File("/home/local/ZOHOCORP/josh-zstch1015/Code_IT_Files/Users/"+userName+"/input.txt");
        try {
            FileWriter writer = new FileWriter(file);
            writer.write(getInput());
            writer.close();
            return true;
        }catch (Exception ee){
            return false;
        }
    }

    public boolean writer(){
        if (writeJavaCode() && writeInput()) {
            return true;
        }
        return false;
    }



}

