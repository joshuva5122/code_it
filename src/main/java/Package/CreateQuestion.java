package Package;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import Package.MySqlFunc;
import Package.QuestionCreator;

public class CreateQuestion extends HttpServlet{
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
		response.addHeader("Access-Control-Allow-Origin", "*");
		PrintWriter printWriter = new PrintWriter(response.getWriter());
		JSONObject jsonObjectRes = new JSONObject();
		MySqlFunc mySql = new MySqlFunc();
		BufferedReader br =request.getReader();
		String data=br.readLine();
		org.json.simple.parser.JSONParser jsonParser = new org.json.simple.parser.JSONParser();
		JSONObject jsonObject = new JSONObject();
		try {
			jsonObject = (JSONObject) jsonParser.parse(data);
		}
		catch(Exception e) {
			System.out.println(e.getMessage());
		}
		String qname = (String) jsonObject.get("qname");
		String question = (String) jsonObject.get("question");
		String bPCode = (String) jsonObject.get("bPCode");
		String input = (String) jsonObject.get("input");
		String output = (String) jsonObject.get("output");
		try {
			if(mySql.addQuestion(qname)) {
				QuestionCreator questionC = new QuestionCreator(qname, question, bPCode, input, output);
				boolean flag = questionC.create();
				jsonObjectRes.put("file", String.valueOf(flag));
				printWriter.print(jsonObjectRes);
			}
			else {
				jsonObjectRes.put("file", "already exists");
				printWriter.print(jsonObjectRes);
			}
		}
		catch(Exception e) {
			jsonObjectRes.put("file", "false");
			printWriter.print(jsonObjectRes);
			System.out.println(e.getMessage());
		}
	}
}


