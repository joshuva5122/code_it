package Package;

import java.io.File;
import java.io.FileWriter;

public class QuestionCreator {

    private String questionName;
    private String question;
    private String bPCode;
    private String input;
    private String output;

    public QuestionCreator(String questionName, String question,String bPCode, String input, String output){
        this.questionName="/home/local/ZOHOCORP/josh-zstch1015/Code_IT_Files/Question_Folder/"+questionName;
        this.question=question;
        this.bPCode=bPCode;
        this.input=input;
        this.output=output;
    }

    private boolean createQuestionFolder(){
        File dir =new File(questionName);
        return dir.mkdir();
        

    }

    private boolean createQuestionFile(){
        File file =new File(questionName+"/question.txt");
        try {
            FileWriter writer = new FileWriter(file);
            writer.write(question);
            writer.close();
            return true;
        }catch (Exception ee){
        	return false;
        }
    }
    
    private boolean createBPCodeFile() {
    	File file =new File(questionName+"/BPCode.txt");
        try {
            FileWriter writer = new FileWriter(file);
            writer.write(bPCode);
            writer.close();
            return true;
        }catch (Exception ee){
        	return false;
        }
    }

    private boolean createInputFile(){
        File file =new File(questionName+"/input.txt");
        try {
            FileWriter writer = new FileWriter(file);
            writer.write(input);
            writer.close();
            return true;
        }catch (Exception ee){
        	return false;
        }
    }

    private boolean createOutputFile(){
        File file =new File(questionName+"/output.txt");
        try {
            FileWriter writer = new FileWriter(file);
            writer.write(output);
            writer.close();
            return true;
        }catch (Exception ee){
        	return false;
        }
    }

    public boolean create(){
        if(createQuestionFolder() && createQuestionFile() && createBPCodeFile() && createInputFile() && createOutputFile()) {
        	return true;
        }
        return false;
    }

}
