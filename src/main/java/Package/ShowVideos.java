package Package;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONObject;

import Package.MySqlFunc;

public class ShowVideos extends HttpServlet{
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
		response.addHeader("Access-Control-Allow-Origin", "*");
		PrintWriter printWriter = new PrintWriter(response.getWriter());
		JSONObject jsonObject = new JSONObject();
		String[] filename = null;
		String[] description = null;
		try {
			filename = MySqlFunc.display("filename", "files").split(",");
			description = MySqlFunc.display("description", "files").split(",");
		}
		catch(Exception e) {
			System.out.println(e.getMessage());
		}
		for(int i = 0; i < filename.length; i++) {
			jsonObject.put(filename[i], description[i]);
		}
		printWriter.print(jsonObject);
	}
}
