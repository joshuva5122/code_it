package Package;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import Package.MySqlFunc;

public class SessionCheck {
	public boolean sessionCheckAdmin(HttpServletRequest request) throws Exception {
		HttpSession session = request.getSession();	
		MySqlFunc mysql = new MySqlFunc();
		String adminname = (String) session.getAttribute("adminname");
		if(mysql.checkData(adminname, "admin", "username", 1)) {
			return true;
		}
		return false;
	}
	public boolean sessionCheckUsers(HttpServletRequest request) throws Exception {
		HttpSession session = request.getSession();	
		MySqlFunc mysql = new MySqlFunc();
		String username = (String) session.getAttribute("username");
		if(mysql.checkData(username, "users", "username", 1)) {
			return true;
		}
		return false;
	}
}
